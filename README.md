![Full-Stack Developer](https://camo.githubusercontent.com/5b1d292467a7b41f288e50d450674ef3cfb99862405c58b6d440957ae3519c22/68747470733a2f2f666972656261736573746f726167652e676f6f676c65617069732e636f6d2f76302f622f666c6578692d636f64696e672e61707073706f742e636f6d2f6f2f64656d706769372d35323066386435662d363364342d343435332d383832322d6462633134396165323766382e6769663f616c743d6d6564696126746f6b656e3d39316330633762322d393363332d343032392d623031312d316138373033633537333064)
### Hi there 👋, I'm Hamim Hossain
#### A Full-Stack Developer

🌐 Full-Stack Skills:
- 💻 Front-End: HTML, CSS, JavaScript, React, Svelte
- 🌐 Back-End: Python, Node.js, FastAPI, Django
- 🗄️ Databases: SQL, MongoDB, Firebase
- 🚀 Deployment & DevOps: Docker, AWS, Heroku, DigitalOcean
- 🔐 Security: Authentication & Authorization
- 📊 APIs: RESTful, GraphQL

🐍 Python Specialization:
- 🧪 Data Science: Pandas, NumPy, Matplotlib
- 🤖 Machine Learning: 🕸️
- 📈 Data Analysis: Jupyter Notebook
- 🐳 Containerization: Docker for Python Applications

Skills: Python / JS / REACT / SVELTE/ HTML / CSS

- 🔭 I’m currently working on Fiverr and UpWork 
- ⚡ Fun fact: I am a self taught Python developer 


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/seracoder)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/dev-dot-to.svg' alt='dev' height='40'>](https://dev.to/seracoder)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/twitter.svg' alt='twitter' height='40'>](https://twitter.com/seracoder)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/stackoverflow.svg' alt='stackoverflow' height='40'>](https://stackoverflow.com/users/19412743)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/icloud.svg' alt='website' height='40'>](seracoder.com)  

[![trophy](https://github-profile-trophy.vercel.app/?username=seracoder)](https://github.com/ryo-ma/github-profile-trophy)

